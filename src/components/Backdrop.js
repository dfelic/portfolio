import React from "react"
import styles from "../css/backdrop.module.css"

const Backdrop = ({ show, hide, children }) =>
  show ? (
    <div className={styles.backdrop} onClick={hide}>
      {children}
    </div>
  ) : null

export default Backdrop
