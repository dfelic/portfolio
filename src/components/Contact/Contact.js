import React from "react"
import Title from "../Title"
import styles from "../../css/contact.module.css"

const Contact = () => {
  return (
    <section className={styles.contact}>
      <Title title="Contact" subtitle="Me" />
      <div className={styles.center}>
        <form action="https://formspree.io/dfeliciano9@gmail.com" method="POST">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            name="name"
            id="name"
            className={styles.formControl}
            placeholder="Full Name"
          />
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            id="email"
            className={styles.formControl}
            placeholder="email@email.com"
          />
          <label htmlFor="message">Message</label>
          <textarea
            name="message"
            id="message"
            rows="10"
            className={styles.formControl}
            placeholder="Include a message"
          />
          <input type="submit" value="Submit Form" className={styles.submit} />
        </form>
      </div>
    </section>
  )
}

export default Contact
