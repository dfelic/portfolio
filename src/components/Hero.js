import React from "react"
import styled from "styled-components"
import BackgroundImage from "gatsby-background-image"

const Hero = ({ img, className, home, work, children }) => {
  return (
    <BackgroundImage fluid={img} className={className}>
      {children}
    </BackgroundImage>
  )
}

export default styled(Hero)`
  background: ${props =>
    props.home
      ? "linear-gradient(rgb(57, 147, 221, 0.55), rgba(0, 0, 0, 0.7))"
      : props.work
      ? "linear-gradient(rgb(57, 147, 221, 0.1), rgba(0, 0, 0, 0.3))"
      : "none"};
  min-height: ${props => (props.home ? "calc(100vh - 62px)" : "60vh")};
  background-position: center;
  background-size: cover;
  opacity: 1 !important;
  display: flex;
  justify-content: center;
  align-items: center;
`
