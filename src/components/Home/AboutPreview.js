import React from "react"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Title from "../Title"
import styles from "../../css/aboutpreview.module.css"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const query = graphql`
  query {
    file(relativePath: { eq: "aquarium.jpg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`

const AboutPreview = () => {
  const {
    file: {
      childImageSharp: { fluid },
    },
  } = useStaticQuery(query)

  return (
    <section className={styles.about}>
      <Title title="about" subtitle="me" />
      <div className={styles.aboutCenter}>
        <article className={styles.aboutImg}>
          <div className={styles.imgContainer}>
            <Img fluid={fluid} />
          </div>
        </article>
        <article className={styles.aboutInfo}>
          <h4> Problem Solver</h4>
          <p>
            I love finding solutions to complex problems. For the last 2 years,
            I've focused on providing local organizations with solutions to
            various business needs or offering improvements to their processes
            through web applications built with React, Node, and Amazon Web
            Services.
          </p>
          <AniLink fade to="/about">
            <button type="button" className="btn-primary">
              Read More
            </button>
          </AniLink>
        </article>
      </div>
    </section>
  )
}

export default AboutPreview
