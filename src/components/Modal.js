import React from "react"
import Backdrop from "../components/Backdrop"
import { FaGlobe, FaGitlab } from "react-icons/fa"
import styles from "../css/modal.module.css"

const Modal = ({ show, hide, details }) => {
  const { src, title, text, tech, gitlab, site } = details
  return show ? (
    <Backdrop show={show} hide={hide}>
      <div className={styles.modal}>
        <img
          src={src}
          className={styles.photo}
          alt="Screenshot of specified project"
        />

        <h1 className={styles.title}>Name: {title}</h1>
        <span className={styles.links}>
          {title !== "Learning With Jasmin" ? (
            <>
              Site:
              <a href={site} target="_blank" rel="noopener noreferrer">
                <FaGlobe />
              </a>
            </>
          ) : (
            <></>
          )}

          {gitlab ? (
            <>
              Gitlab project:
              <a href={gitlab} target="_blank" rel="noopener noreferrer">
                <FaGitlab />
              </a>
            </>
          ) : (
            <></>
          )}
        </span>
        <p>
          <b>Description:</b> {text}
        </p>
        <p>
          <b>Technologies I Used:</b> {tech}
        </p>
      </div>
    </Backdrop>
  ) : (
    <></>
  )
}

export default Modal
