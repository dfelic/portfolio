import React from "react"
import styles from "../../css/project.module.css"
import techIcons from "../../constants/tech-icons"
import ProjectLinks from "./ProjectLinks"
import Image from "gatsby-image"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import gatsbyIcon from "../../images/gatsby.ico"

const Project = ({ details }) => {
  const {
    previewImage: { fluid },
    title,
    gitlabLink,
    siteLink,
    tools,
    slug,
  } = details

  return (
    <article className={styles.container}>
      <div className={styles.preview}>
        <Image fluid={fluid} alt="Single project" />
        {/* <AniLink fade to={`/tours/${slug}`} className={styles.link}>
          Details
        </AniLink> */}
      </div>

      <div className={styles.details}>
        <div className={styles.detailGroup}>
          <p className={styles.title}>{title}</p>
          <ProjectLinks site={siteLink} gitlab={gitlabLink} />
        </div>
        <div className={styles.detailGroup}>
          <ul className={styles.icons}>
            {techIcons.map((icon, index) => {
              return tools.includes(icon.text) ? (
                <li key={index}>{icon.icon}</li>
              ) : (
                <></>
              )
            })}
            {title === "Portfolio" ? (
              <img src={gatsbyIcon} alt="Gatsby icon" />
            ) : (
              <></>
            )}
          </ul>
          <div className={styles.projectLink}>
            <AniLink fade to={`/work/${slug}`} className="btn-primary">
              Learn More
            </AniLink>
          </div>
        </div>
      </div>
    </article>
  )
}

export default Project
