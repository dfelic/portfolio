import React from "react"
import { FaGlobe, FaGitlab } from "react-icons/fa"
import styles from "../../css/projectlinks.module.css"

const ProjectLinks = ({ site, gitlab, page }) => {
  return (
    <div className={styles.links}>
      {site ? (
        <>
          {page ? "See the site: " : <></>}
          <a href={site} target="_blank" rel="noopener noreferrer">
            <FaGlobe />
          </a>
        </>
      ) : (
        <></>
      )}
      {gitlab ? (
        <>
          {page ? "See the code: " : <></>}
          <a href={gitlab} target="_blank" rel="noopener noreferrer">
            <FaGitlab />
          </a>
        </>
      ) : (
        <></>
      )}
    </div>
  )
}

export default ProjectLinks
