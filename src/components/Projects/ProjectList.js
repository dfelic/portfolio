import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import styles from "../../css/projectlist.module.css"
import Project from "./Project"

const getProjects = graphql`
  {
    projects: allContentfulProject(sort: { fields: tools, order: DESC }) {
      edges {
        node {
          title
          previewImage {
            fluid {
              ...GatsbyContentfulFluid_tracedSVG
            }
          }
          gitlabLink
          siteLink
          slug
          tools
          contentful_id
        }
      }
    }
  }
`

const ProjectList = () => {
  const { projects } = useStaticQuery(getProjects)
  return (
    <section className={styles.projects}>
      {projects.edges.map(({ node }) => (
        <Project key={node.contentful_id} details={node} />
      ))}
    </section>
  )
}

export default ProjectList
