import React from "react"
import styles from "../css/techIcon.module.css"

const techIcon = ({ icon, children }) => {
  return (
    <div className={styles.container}>
      <span className={styles.techIcon}>{icon}</span>
      {children}
    </div>
  )
}

export default techIcon
