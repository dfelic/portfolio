export default [
  {
    path: "/",
    text: "home",
  },
  {
    path: "/about",
    text: "about",
  },
  {
    path: "/work",
    text: "work",
  },
  {
    path: "/contact",
    text: "contact",
  },
]
