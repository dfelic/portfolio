import React from "react"
import { FaLinkedin, FaGitlab } from "react-icons/fa"

export default [
  {
    icon: <FaLinkedin />,
    url: "https://www.linkedin.com/in/dan-feliciano/",
  },
  {
    icon: <FaGitlab />,
    url: "https://gitlab.com/dfelic",
  },
]
