import React from "react"
import {
  FaHtml5,
  FaCss3,
  FaReact,
  FaNode,
  FaAws,
  FaPython,
} from "react-icons/fa"

export default [
  {
    icon: <FaHtml5 style={{ color: "#E86332" }} />,
    text: "HTML5",
  },
  {
    icon: <FaCss3 style={{ color: "#2A93E9" }} />,
    text: "CSS",
  },
  {
    icon: <FaReact style={{ color: "#24D0F4" }} />,
    text: "ReactJS",
  },
  {
    icon: <FaNode />,
    text: "Node.js",
  },
  {
    icon: <FaAws />,
    text: "Amazon Web Services",
  },
  {
    icon: <FaPython />,
    text: "Python",
  },
]
