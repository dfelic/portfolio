import React from "react"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import styles from "../css/error.module.css"
import Layout from "../components/Layout"
import Banner from "../components/Banner"
import SEO from "../components/SEO"

const error = () => {
  return (
    <Layout>
      <SEO page="404" />
      <header className={styles.error}>
        <Banner title="Oops, dead end...">
          <AniLink fade to="/" className="btn-white">
            Back to Home
          </AniLink>
        </Banner>
      </header>
    </Layout>
  )
}

export default error
