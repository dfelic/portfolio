import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import Layout from "../components/Layout"
import Hero from "../components/Hero"
import Banner from "../components/Banner"
import styles from "../css/about.module.css"
import techIcons from "../constants/tech-icons"
import TechIcon from "../components/TechIcon"
import SEO from "../components/SEO"

export default function about({ data }) {
  return (
    <Layout>
      <SEO page="About" />
      <Hero img={data.heroImg.childImageSharp.fluid}>
        <Banner title="About me" />
      </Hero>
      <section className={styles.body}>
        <article className={styles.photo}>
          <Img fluid={data.aboutImg.childImageSharp.fluid} />
        </article>
        <div className={styles.text}>
          <h2>I'm a software developer from Columbia, South Carolina.</h2>
          <h4>
            I spend most of my time on web development working with HTML, CSS,
            React, Node, and Amazon Web Services.
          </h4>
          <p>
            I enjoy the challenges of problem solving. Digging into the the
            details, leveraging new solutions, and pushing myself to learn
            something new each day is exciting to me, especially when I
            successfully apply these new concepts in my solutions. When I'm not
            coding you will often find me out training to run a half marathon
            this year or looking for a new adventure.
          </p>
          <ul className={styles.list}>
            <li>
              <b>Fun Hobbies:</b> Weiqi/Go/Baduk, D&D, Board Games
            </li>
            <li>
              <b>Currently Learning:</b> Chess
            </li>
          </ul>
        </div>
      </section>

      <h1 className={styles.heading}>
        My <span>Stack</span>
      </h1>
      <div className={styles.techStack}>
        {techIcons.map((item, index) =>
          item.text !== "Python" ? (
            <TechIcon icon={item.icon} key={index}>
              {item.text}
            </TechIcon>
          ) : (
            <></>
          )
        )}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    heroImg: file(relativePath: { eq: "asheville.jpg" }) {
      childImageSharp {
        fluid(quality: 90, maxWidth: 4160) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
    aboutImg: file(relativePath: { eq: "linkedIn.jpg" }) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`
