import React from "react"
// import { graphql } from "gatsby"
import Layout from "../components/Layout"
// import Hero from "../components/Hero"
// import Banner from "../components/Banner"
import SEO from "../components/SEO"
import Contact from "../components/Contact/Contact"

export default function contact() {
  return (
    <Layout>
      <SEO page="Contact" />
      {/* <Hero>
        <Banner title="Contact Me" />
      </Hero> */}
      <Contact />
    </Layout>
  )
}

// export const query = graphql``
