import React from "react"
import { graphql } from "gatsby"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Layout from "../components/Layout"
import Hero from "../components/Hero"
import Banner from "../components/Banner"
import AboutPreview from "../components/Home/AboutPreview"
import SEO from "../components/SEO"

export default ({ data }) => (
  <Layout>
    <SEO page="Home" />
    <Hero img={data.heroImg.childImageSharp.fluid} home={true}>
      <Banner title="Software Developer" info="Welcome to my portfolio site!">
        <AniLink fade to="/work" className="btn-white">
          View Work
        </AniLink>
      </Banner>
    </Hero>
    <AboutPreview />
  </Layout>
)

export const query = graphql`
  query {
    heroImg: file(relativePath: { eq: "temp.jpeg" }) {
      childImageSharp {
        fluid(quality: 90, maxWidth: 4160) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`
