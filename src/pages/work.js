import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/Layout"
import styles from "../css/work.module.css"
import Hero from "../components/Hero"
import Banner from "../components/Banner"
import SEO from "../components/SEO"
import Title from "../components/Title"
import ProjectList from "../components/Projects/ProjectList"

const work = ({ data }) => {
  return (
    <Layout>
      <SEO page="Work" />
      <Hero img={data.heroImg.childImageSharp.fluid} work={true}>
        <Banner title="My Work" />
      </Hero>
      <section className={styles.body}>
        <Title title="Recent" subtitle="Projects" />
        <div className={styles.projects}>
          <ProjectList />
        </div>
      </section>
    </Layout>
  )
}

export default work

export const query = graphql`
  query {
    heroImg: file(relativePath: { eq: "workHero.jpg" }) {
      childImageSharp {
        fluid(quality: 90, maxWidth: 4160) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`
