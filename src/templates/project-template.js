import React from "react"
import { graphql } from "gatsby"
import AniLink from "gatsby-plugin-transition-link/AniLink"
import Layout from "../components/Layout"
import SEO from "../components/SEO"
import Hero from "../components/Hero"
import ProjectLinks from "../components/Projects/ProjectLinks"
import styles from "../css/template.module.css"

const Template = ({ data }) => {
  const {
    title,
    siteLink,
    gitlabLink,
    tools,
    highlights,
    description: { description },
    previewImage: { fluid },
  } = data.project
  return (
    <Layout>
      <SEO page={title} />
      <Hero img={fluid} />
      <section className={styles.template}>
        <div className={styles.container}>
          <h2>{title}</h2>
          <ProjectLinks site={siteLink} gitlab={gitlabLink} page={true} />
          <h4>Tools: {tools}</h4>
          <p>{description}</p>
          <h4>{highlights.title}</h4>
          <ul>
            {highlights.taskList.map((task, index) => (
              <li key={index} className={styles.task}>
                {task}
              </li>
            ))}
          </ul>
        </div>
        <AniLink fade to="/work" className="btn-primary">
          Return to work
        </AniLink>
      </section>
    </Layout>
  )
}

export default Template

export const query = graphql`
  query($slug: String!) {
    project: contentfulProject(slug: { eq: $slug }) {
      title
      siteLink
      gitlabLink
      tools
      description {
        description
      }
      highlights {
        title
        taskList
      }
      previewImage {
        fluid {
          ...GatsbyContentfulFluid_tracedSVG
        }
      }
    }
  }
`
